"use strict";

function displayProducts(){
    let ddlSelectedTypes = document.getElementById("type");
    ddlSelectedTypes = ddlSelectedTypes.value;

    $('#list').empty();
    $('#description').empty();

    if(ddlSelectedTypes === "valueTypeNone"){
        $('#list').empty();
        $('#description').empty();
    } else {
        $('#list').load("app/Public/FormProducts/" + ddlSelectedTypes.toLowerCase() + 'Form' + '.php');
        // let description = document.getElementById(ddlSelectedTypes + "_Description").value;
        // $('#description').append("<p>" + description +"</p>");
    }

}


function cancelSubmission() {
    location.href = "index.php";   
}

function submitProduct(){
    let returnResult = formProcess();
    if(returnResult == false){
        document.getElementById("productSubmissionForm").submit();
    }
    

}

function addProduct(){
    location.href = "add.php";
}

function formProcess(){
    let mainContainer = {
        sku: "skuError",
        name: "nameError",
        price: "priceError",
    };

    let ddlSelectedTypes = document.getElementById("type");
    ddlSelectedTypes = ddlSelectedTypes.value;

    switch (ddlSelectedTypes) {
        case "valueTypeNone":
            mainContainer['valueTypeNone'] = "typeError";
            return makeErrorVisible(mainContainer);
        case "DVD":
            mainContainer['dvdInput'] = "dvdError";
            return makeErrorVisible(mainContainer);
        case "Book":
            mainContainer['bookInput'] = "bookError";
            return makeErrorVisible(mainContainer);
        case "Furniture":
            mainContainer['height'] = "heightError";
            mainContainer['width'] = "widthError";
            mainContainer['length'] = "lengthError";
            return makeErrorVisible(mainContainer);
        default:
            break;
    }



    
}

function makeErrorVisible(mainContainer){
    for(const [key, value] of Object.entries(mainContainer)){
        $(`#${value}`).empty();
    }
    let emtpyFieldStatus = [];
    for(const [key, value] of Object.entries(mainContainer)){
        let input = document.getElementById(key);
        let error = document.getElementById(value);

        if(input.value === "" || input.value === "valueTypeNone"){
            emtpyFieldStatus.push(true);
            if(input.id === "dvdInput"){
                error.textContent = "Please fill the dvd size";
            }else if(input.id === "bookInput"){
                error.textContent = "Please fill the book weight";
            } else if(input.id === "width" || input.id === "height" || input.id === "length"){
                error.textContent = "Please provide dimensions";
            } else if (input.id === "valueTypeNone"){
                error.textContent = "Please choose a type for the product";
            }else{
                error.textContent = "Please fill in the field with correct data";
            }
            error.style.color = "Red";
        }    
    }
    if(emtpyFieldStatus.length > 0){
        return true;
    } else {
        return false;
    }
}

function deleteProduct(){
    let form = document.getElementById("deletionForm");
    form.submit();
    
}