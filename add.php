<?php
require_once(__DIR__ . "/vendor/autoload.php");
use App\Database\Db as Db;
$Db = new Db();
$result = $Db->select("SELECT * FROM product_type");
$a = "a debug-stop string";

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
    <script src="script.js"></script>


</head>
<body>
    <div class="container p-3">
        <div class="row">
            <h1 class="col-sm">Product Add</h1>
            <input class="col-sm-2 btn btn-success m-2" type="submit" name="saveProduct" value="Save" id="submitProduct" onclick="submitProduct()">
            <input class="col-sm-2 btn btn-danger m-2" type="button" name="cancelProductSubmission" value="Cancel" id="cancelProductSubmission" onclick="cancelSubmission()">
        </div>
    </div>
    <form class="p-6 m-5" action="app/FormHandling/AddForm.php" method="POST" id="productSubmissionForm" name="productSubmissionForm" onsubmit="return formProcess()" enctype="multipart/form-data">
        <div class="form-group row mt-5 mb-2">
            <div class="col-sm-1">
                <label for="sku" class="col-sm-2 col-form-label">SKU</label>
            </div>
            <div class="col-sm-2">
                <input class="form-control" type="text" name="sku" id="sku" required>
                <span id="skuError"></span>
            </div>
        </div>

        <div class="form-group row mb-2">
            <div class="col-sm-1">
                <label for="name" class="col-sm-2 col-form-label">Name</label>
            </div>
            <div class="col-sm-2">
                <input class="form-control" type="text" name="name" id="name" requierd>
                <span id="nameError"></span>
            </div>
        </div>
        <div class="form-group row mb-2">
            <div class="col-sm-1">
                <label for="price" class="col-sm-1 col-form-label">Price($)</label>
            </div>
            <div class="col-sm-2">
                <input class="form-control" type="number" name="price" id="price" requierd>
                <span id="priceError"></span>
            </div>
        </div>
        <div class="form-group row mb-2">
            <div class="col-sm-1">
                <label for="type" class="col-sm col-form-label">Type Switcher</label>
            </div>
            <div class="col-sm-2">
                <select name="type" id="type" onchange="displayProducts()">
                <?php
                    if(!empty($result)){
                        foreach($result as $row){
                            if($row["id"] == 1){ ?>
                                <option value="valueTypeNone" id="valueTypeNone" selected><?= $row["type"] ?></option>           
                            <?php } else {?>
                            <option value="<?= $row["type"] ?>" id="<?= $row["id"] ?>" ><?= $row["type"] ?></option>        
                        <?php }
                        }
                    }?>
                </select>
            </div>
            <span id="typeError"></span>
        </div>
        
        <div class="form-group row mb-2 p-4" id="list"></div>
        <div class="form-group row mb-2 p-4" id="description"></div>
        </form>
</body>
</html>