<?php
require_once(__DIR__ . "/vendor/autoload.php");
use App\Database\Db as Db;
use App\Factory\FactoryProducts\FactoryBook as FactoryBook;
use App\Factory\FactoryProducts\FactoryDvd as FactoryDvd;
use App\Factory\FactoryProducts\FactoryFurniture as FactoryFurniture;

//Names of tables from db to add them all to the arrays and then fetch it to a user
$Db = new Db();
$tables = ["book" => array(), "dvd" => array(), "furniture" => array()];

// $mixedArray = [];

foreach($tables as $key => $value){
    $sqlArray = $Db->select("SELECT * FROM " . $key);
    
    foreach($sqlArray as $key2 => $value2){
        $className = "Factory" . ucfirst($key);
        $className = "App\\Factory\\FactoryProducts\\" . $className;
        $classNfunction = $className::objectCreate($value2);
        $getClassDescription = $classNfunction->getObjData();
        array_push($tables[$key], $getClassDescription);
    }
}
$a = "a debug-stop string";
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Product List</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
    <script src="script.js"></script>
</head>
<body>
    <div class="container p-3">
        <div class="row">
            <h1 class="col-sm">Product List</h1>
            <input class="col-sm-2 btn btn-light border m-2" type="button" name="addProduct" value="Add" onclick="addProduct()" id="addProduct">
            <input class="col-sm-2 btn btn-light border m-2" type="button" name="massDelete" value="Mass delete" onclick="deleteProduct()">
        </div>
    </div>

    <form action="app/FormHandling/DeleteForm.php" method="post" id="deletionForm">
        <div class="container">
            <div class="row row-cols-5">
            <!-- Here goes the for-loop -->
            <?php foreach($tables as $key => $value){ 
                    foreach($value as $key2 => $value2){?>
                <div class="column bg-light border p-1 m-2">
                    <div class="container">
                        <div class="row">
                            <div class="position-relative">
                                <div class="position-absolute top-0 start-0">
                                    <div class="col-sm">
                                        <input type="checkbox" name="deleteMark_type_<?= $value2["type"] ?>_id_<?= $value2["id"] ?>" >
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row p-4 text-center">
                            <?php   foreach($value2 as $characteristics => $charVal){
                                if($characteristics == "id" || $characteristics == "type"){
                                    continue;
                                } else {
                                    echo "<p>". $charVal ."</p>";
                                }}
                            ?>
                        </div>
                    </div>
                </div>
            <?php }} ?>
            </div>
        </div>
    </form>
</body>
</html>
<?php 




?>