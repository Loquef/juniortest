CREATE DATABASE productlist;
use productList;

create table product_type(
    id int NOT NULL AUTO_INCREMENT PRIMARY KEY,
    type varchar(255),
    description longtext
);

create table furniture (
    id int NOT NULL PRIMARY KEY AUTO_INCREMENT,
    sku varchar(255) NOT NULL,
    name varchar(255) NOT NULL,
    price float NOT NULL,
    height int,
    width int,
    length int
);

create table book (
    id int NOT NULL PRIMARY KEY AUTO_INCREMENT,
    sku varchar(255) NOT NULL,
    name varchar(255) NOT NULL,
    price float NOT NULL,
    weight float
    
);

create table dvd (
    id int NOT NULL PRIMARY KEY AUTO_INCREMENT,
    sku varchar(255) NOT NULL,
    name varchar(255) NOT NULL,
    price float NOT NULL,
    size int
);

    
INSERT INTO product_type (type) VALUES ("...");
INSERT INTO product_type (type, description) VALUES ("DVD", "DVD (abbreviation for Digital Versatile Disc or Digital Video Disc) is a digital optical disc data storage format invented and developed in 1995 and released in late 1996. The medium can store any kind of digital data and was widely used for software and other computer files as well as video programs watched using DVD players. DVDs offer higher storage capacity than compact discs while having the same dimensions.");
INSERT INTO product_type (type, description) VALUES ("Book" , "A book is a medium for recording information in the form of writing or images, typically composed of many pages (made of papyrus, parchment, vellum, or paper) bound together and protected by a cover. The technical term for this physical arrangement is codex (plural, codices). In the history of hand-held physical supports for extended written compositions or records, the codex replaces its predecessor, the scroll. A single sheet in a codex is a leaf and each side of a leaf is a page.");
INSERT INTO product_type (type, description) VALUES ("Furniture", "Furniture refers to movable objects intended to support various human activities such as seating (e.g., chairs, stools, and sofas), eating (tables), and sleeping (e.g., beds). Furniture is also used to hold objects at a convenient height for work (as horizontal surfaces above the ground, such as tables and desks), or to store things (e.g., cupboards and shelves). Furniture can be a product of design and is considered a form of decorative art. In addition to furniture's functional role, it can serve a symbolic or religious purpose. It can be made from many materials, including metal, plastic, and wood. Furniture can be made using a variety of woodworking joints which often reflect the local culture.");
