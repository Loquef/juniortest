<?php
namespace App\Database;

use mysqli;

class Db{
    private $connection = null;

    public function __construct()
    {
        $this->connection = new mysqli ("localhost", "root", "root", "productlist");
    }

    public function select($sql){
        $resultArray = [];
        $stmt = $this->connection->prepare($sql);
        $stmt->execute();
        $result = $stmt->get_result();
        while($row = $result->fetch_assoc()){
            array_push($resultArray, $row);
        }
        
        return $resultArray;
    }

    public function save($objectArray){
        if($objectArray["id"] == ""){
            unset($objectArray["id"]);    
        }
        $sql = "INSERT INTO " . $this->connection->real_escape_string($objectArray["type"]) . " SET ";
        unset($objectArray["type"]);
        foreach($objectArray as $key => $value){
            $sql .= $this->connection->real_escape_string($key) . '= "' . $this->connection->real_escape_string($value) . '",';
        }
        $sql = rtrim($sql, ",");

        $this->connection->query($sql);
    }

    public function remove($sql){
        $this->connection->query($sql);
    }
}