<?php
namespace App\Interfaces;

interface IGetObjectData{
    public function getObjData() : array;
}