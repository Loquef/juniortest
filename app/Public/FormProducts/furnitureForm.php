<?php 
require_once(__DIR__ . "../../../../vendor/autoload.php");
use App\Database\Db as Db;
$Db = new Db();
$result = $Db->select("SELECT description FROM product_type WHERE type='furniture'");
?>

<div class="form-group row mb-2">
    <div class="col-sm-1">
        <label class="col-form-label" for="height">Height (CM)</label>
    </div>
    <div class="col-sm-2">
        <input class="form-control" type="number" name="height" id="height">
        <span id="heightError"></span>
    </div>
</div>
<div class="form-group row mb-2">
    <div class="col-sm-1">
        <label class="col-form-label" for="width">Width (CM)</label>
    </div>
    <div class="col-sm-2">
        <input class="form-control" type="number" name="width" id="width">
        <span id="widthError"></span>
    </div>
</div>

<div class="form-group row mb-2">
    <div class="col-sm-1">
        <label class="col-form-label" for="length">Length (CM)</label>
    </div>
    <div class="col-sm-2">
        <input class="form-control" type="number" name="length" id="length">
        <span id="lengthError"></span>
</div>

<div class="form-group row mb-2 p-4" id="description">
    <p><?= $result[0]["description"] ?></p>
</div>
