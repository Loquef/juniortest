<?php 
require_once(__DIR__ . "../../../../vendor/autoload.php");
use App\Database\Db as Db;
$Db = new Db();
$result = $Db->select("SELECT type, description FROM product_type WHERE type='book'");


?>
<div class="col-sm-1">
    <label class="col-form-label" for="book">Weight (KG)</label>
</div>
<div class="col-sm-2">
    <input class="form-control" type="number" name="weight" id="bookInput">
    <span id="bookError"></span>
</div>

<div class="form-group row mb-2 p-4" id="description">
    <p><?= $result[0]["description"] ?></p>
</div>
