<?php
namespace App\Factory\FactoryProducts;
require_once(__DIR__ . "./../../../vendor/autoload.php");


use App\Products\ConcreteProducts\Dvd as Dvd;
use App\Factory\ProductFactory as ProductFactory;
use App\Products\Product as Product;

class FactoryDvd implements ProductFactory{
    public static function objectCreate($postReq) : Product
    {
        return new Dvd($postReq["sku"], $postReq["name"], $postReq["price"], $postReq["size"], $postReq["id"]);
    }
}