<?php
namespace App\Factory\FactoryProducts;
require_once(__DIR__ . "./../../../vendor/autoload.php");

use App\Products\ConcreteProducts\Book as Book;
use App\Factory\ProductFactory as ProductFactory;
use App\Products\Product as Product;

class FactoryBook implements ProductFactory{
    public static function objectCreate($postReq) : Product
    {
        return new Book($postReq["sku"], $postReq["name"], $postReq["price"], $postReq["weight"], $postReq["id"]);
    }
}
