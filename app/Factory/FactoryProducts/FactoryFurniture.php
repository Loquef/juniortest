<?php
namespace App\Factory\FactoryProducts;
require_once(__DIR__ . "./../../../vendor/autoload.php");


use App\Products\ConcreteProducts\Furniture as Furniture;
use App\Factory\ProductFactory as ProductFactory;
use App\Products\Product as Product;

class FactoryFurniture implements ProductFactory{
    public static function objectCreate($postReq) : Product
    {
        return new Furniture($postReq["sku"], $postReq["name"], $postReq["price"], $postReq["height"], $postReq["width"], $postReq["length"], $postReq["id"]);
    }
}