<?php
namespace App\Factory;

require_once(__DIR__ . "./../Products/Product.php");
use App\Products\Product as Product;

interface ProductFactory{
    public static function objectCreate(array $postReq) : Product;
}