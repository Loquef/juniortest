<?php 
namespace App\Products;

abstract class Product{
    protected $id;
    protected $sku;
    protected $name;
    protected $price;

    abstract public function getId();

    abstract public function getSku();

    abstract public function getName();

    abstract public function getPrice();

    abstract public function getObjData() : array;
}

