<?php 
namespace App\Products\ConcreteProducts;
require_once(__DIR__ . "../../../../vendor/autoload.php");

use App\Products\Product as Product;
use App\Interfaces\IGetObjectData as IGetObjectData;

class Book extends Product implements IGetObjectData{
    private $weight;
    
    public function __construct($sku, $name, $price, $weight, $id = "")
    {
        $this->id = $id;
        $this->sku = $sku;
        $this->name = $name;
        $this->price = $price;
        $this->weight = $weight;
    }

    public function getObjData() : array
    {
        if($this->getId() == ""){
            $objectData = [
                "id"        =>  $this->getId(),
                "sku"       =>  $this->getSku(),
                "name"      =>  $this->getName(),
                "price"     =>  $this->getPrice(),
                "weight"    =>  $this->getWeight(),
                "type"      =>  "book"
            ];
        } else{
            $objectData = [
                "id"        =>  $this->getId(),
                "sku"       =>  $this->getSku(),
                "name"      =>  $this->getName(),
                "price"     =>  $this->getPrice() . "$",
                "weight"    =>  "Weight: " . $this->getWeight() . "Kg",
                "type"      =>  "book"
            ];
        }
         return $objectData;
    }

    

    public function getId()
    {
        return $this->id;
    }
    public function getSku()
    {
        return $this->sku;
    }
    public function getName()
    {
        return $this->name;
    }
    public function getPrice()
    {
        return $this->price;
    }
    public function getWeight()
    {
        return $this->weight;
    }
}