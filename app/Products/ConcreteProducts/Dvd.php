<?php 
namespace App\Products\ConcreteProducts;
require_once(__DIR__ . "../../../../vendor/autoload.php");

use App\Products\Product as Product;
use App\Interfaces\IGetObjectData as IGetObjectData;

class Dvd extends Product implements IGetObjectData{
    private $size;
    
    public function __construct($sku, $name, $price, $size, $id = "")
    {
        $this->id = $id;
        $this->sku = $sku;
        $this->name = $name;
        $this->price = $price;
        $this->size = $size;
    }

    public function getObjData() : array
    {
        if($this->getId() == ""){
            $objectData = [
                "id"        =>  $this->getId(),
                "sku"       =>  $this->getSku(),
                "name"      =>  $this->getName(),
                "price"     =>  $this->getPrice(),
                "size"      =>  $this->getSize(),
                "type"      =>  "dvd"
            ];
        } else {
            $objectData = [
                "id"        =>  $this->getId(),
                "sku"       =>  $this->getSku(),
                "name"      =>  $this->getName(),
                "price"     =>  $this->getPrice()  . "$",
                "size"      =>  "Size: " . $this->getSize() . "Mb",
                "type"      =>  "dvd"
            ];
        }

        return $objectData;
    }
    public function getId()
    {
        return $this->id;
    }
    public function getSku()
    {
        return $this->sku;
    }
    public function getName()
    {
        return $this->name;
    }
    public function getPrice()
    {
        return $this->price;
    }
    public function getSize()
    {
        return $this->size;
    }
}