<?php 
namespace App\Products\ConcreteProducts;
require_once(__DIR__ . "../../../../vendor/autoload.php");

use App\Products\Product as Product;
use App\Interfaces\IGetObjectData as IGetObjectData;

class Furniture extends Product implements IGetObjectData{
    private $height;
    private $width;
    private $length;
    
    
    public function __construct($sku, $name, $price, $height, $width, $length, $id = "")
    {
        $this->id = $id;
        $this->sku = $sku;
        $this->name = $name;
        $this->price = $price;
        $this->height = $height;
        $this->width = $width;
        $this->length = $length;
    }

    public function getObjData() : array
    {
        if($this->getId() == ""){
            $objectData = [
                "id"        =>  $this->getId(),
                "sku"       =>  $this->getSku(),
                "name"      =>  $this->getName(),
                "price"     =>  $this->getPrice(),
                "height"    =>  $this->getHeight(),
                "width"     =>  $this->getWidth(),
                "length"    =>  $this->getLength(),
                "type"      =>  "furniture"
            ];
        } else {
            $objectData = [
                "id"        =>  $this->getId(),
                "sku"       =>  $this->getSku(),
                "name"      =>  $this->getName(),
                "price"     =>  $this->getPrice()  . "$",
                "Dimentions"    =>  "Dimentions: " . $this->getHeight() . "x" . $this->getWidth() . "x" . $this->getLength(),
                "type"      =>  "furniture"
            ];
        }

        return $objectData;
    }
    public function getId()
    {
        return $this->id;
    }
    public function getSku()
    {
        return $this->sku;
    }
    public function getName()
    {
        return $this->name;
    }
    public function getPrice()
    {
        return $this->price;
    }
    public function getHeight()
    {
        return $this->height;
    }
    public function getWidth()
    {
        return $this->width;
    }
    public function getLength()
    {
        return $this->length;
    }
    
}